<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; margin-top: 10%;">

  <label>Имя</label><br>
  <input class="form-control" type="text" name="firstname" value="<?= $author['firstname'] ? $author['firstname'] : ""?>"><br>
  <label>Фамилия</label><br>
  <input class="form-control" type="text" name="lastname" value="<?= $author['lastname'] ? $author['lastname'] : ""?>"><br>
  <label>Отчество</label><br>
  <input class="form-control" type="text" name="patronymic" value="<?= $author['patronymic'] ? $author['patronymic'] : ""?>"><br>
  <br>
  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/index.php?function=authors'">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
