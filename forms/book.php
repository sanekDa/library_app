<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; margin-top: 10%;">

  <label>Автор</label><br>
  <select name="author_id" class="form-control">
    <?php
    foreach ($authors as $author)
    {
      echo '<option value="' . $author['id'] . '"' . ($author['id'] === $book['author_id'] ? ' selected' : '') . '>';
      echo $author['firstname'] . ' ' . $author['lastname'].' '.$author['patronymic'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

  <label>Жанр</label><br>
  <select name="genre_id" class="form-control">
    <?php
    foreach ($genres as $genre)
    {
      echo '<option value="' . $genre['id'] . '"' . ($genre['id'] === $book['genre_id'] ? ' selected' : '') . '>';
      echo $genre['name'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

  <label>Название</label><br>
  <input class="form-control" type="text" name="name" value="<?= $book['name'] ? $book['name'] : ""?>"><br>

  <label>Дополнительная информация</label><br>
  <input class="form-control" type="text" name="extra_information" value="<?= $book['extra_information'] ? $book['extra_information'] : "" ?>"><br>

  <label>Наличие</label><br>
  <input class="form-control" type="text" name="availability" value="<?= $book['availability'] ? $book['availability'] : "1"?>"><br>
  
  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="history.back()">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
