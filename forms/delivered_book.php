<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; margin-top: 10%;">

  <label>ID выдачи</label><br>
  <select name="delivery_id" class="form-control">
    <?php
    foreach ($deliveries as $delivery)
    {
      echo '<option value="' . $delivery['id'] . '"' . ($delivery['id'] === $item['delivery_id'] ? ' selected' : '') . '>';
      echo $delivery['id'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

  <label>ID книги</label><br>
  <select name="book_id" class="form-control">
    <?php
    foreach ($books as $book)
    {
      echo '<option value="' . $book['id'] . '"' . ($book['id'] === $item['book_id'] ? ' selected' : '') . '>';
      echo $book['id'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>
  
  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="history.back()">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
