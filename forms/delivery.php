<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; margin-top: 10%;">

  <label>Читатель</label><br>
  <select name="client_id" class="form-control">
    <?php
    foreach ($clients as $client)
    {
      echo '<option value="' . $client['id'] . '"' . ($client['id'] == $delivery['client_id'] ? ' selected' : '') . '>';
      echo $client['firstname'] . ' ' . $client['lastname'].' '.$client['patronymic'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>
  <label>Работник</label><br>
  <select name="worker_id" class="form-control">
    <?php
    foreach ($workers as $worker)
    {
      echo '<option value="' . $worker['id'] . '"' . ($worker['id'] == $delivery['worker_id'] ? ' selected' : '') . '>';
      echo $worker['firstname'] . ' ' . $worker['lastname'].' '.$worker['patronymic'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>
  <label>Дата выдачи</label><br>
  <input class="form-control" type="date" name="date_of_delivery" value="<?= $delivery['date_of_delivery'] ?>"><br>
  <label>Дата возврата</label><br>
  <input class="form-control" type="date" name="date_of_return" value="<?= $delivery['date_of_return'] ?>"><br>

  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="history.back()">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
