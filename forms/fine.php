<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; margin-top: 10%;">

  <label>ID доставки</label><br>
  <select name="delivery_id" class="form-control">
    <?php
    foreach ($deliveries as $delivery)
    {
      echo '<option value="' . $delivery['id'] . '"' . ($delivery['id'] === $fine['delivery_id'] ? ' selected' : '') . '>';
      echo $delivery['id'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

 	<label>Описание</label><br>
	<input class="form-control" type="text" name="description" value="<?= $fine['description']? $fine['description']:'' ?>"><br>
	<label>Цена</label><br>
	<input class="form-control" type="text" name="price" value="<?= $fine['price']? $fine['price']:'' ?>"><br>
  <br>
  <br>
  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="history.back()">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
