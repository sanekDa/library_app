<form action="<?= $url ?>" method="post" style="width: 50%; margin-left: 25%; ">

  <label>Имя</label><br>
  <input class="form-control" type="text" name="firstname" class="form-control" value="<?= $worker['firstname'] ?>"><br>

  <label>Фамилия</label><br>
  <input class="form-control" type="text" name="lastname" value="<?= $worker['lastname'] ?>"><br>

  <label>Отчество</label><br>
  <input class="form-control" type="text" name="patronymic" value="<?= $worker['patronymic'] ?>"><br>

  <label>Дата рождения</label><br>
  <input class="form-control" type="date" name="dbirth" value="<?= $worker['dbirth'] ?>"><br>

  <label>Адрес</label><br>
  <input class="form-control" type="text" name="adress" value="<?= $worker['adress'] ?>"><br>

  <label>Номер телефона</label><br>
  <input class="form-control" type="text" name="phone_number" value="<?= $worker['phone_number'] ?>"><br>

  <label>E-mail</label><br>
  <input class="form-control" type="text" name="email" value="<?= $worker['email'] ?>"><br>

  <button class="btn btn-primary" type="submit">Сохранить</button>
  <button class="btn btn-primary" type="button" onclick="history.back()">Назад</button>
  <button class="btn btn-primary" type="button" onclick="window.location='/'">В главное меню</button>
</form>
