<? include 'resources/header.php'; ?>
<h1 class="display-1" align="center">Управление бд "Библиотека"</h1>
<body style="margin-left: 20%; width: 60%" align="center">
	<?php
		include ('resources/functions.php');
		if ($_GET){
			switch ($_GET['function']) {
	            case 'authors':
	                $Functions->show_authors_list();
	                break;
	           	case 'genres':
	                $Functions->show_genres_list();
	                break;
                case 'books':
	                $Functions->show_books_list();
	                break;
	           	case 'fines':
	                $Functions->show_fines_list();
	                break;
	            case 'deliveries':
	                $Functions->show_deliveries_list();
	                break;
	           	case 'delivered_books':
	                $Functions->show_delivered_books_list();
	                break;
                case 'clients':
	                $Functions->show_clients_list();
	                break;
                case 'workers':
	                $Functions->show_workers_list();
	                break;
            }
		}else{
			$Functions->show_main_list();
		}
	?>
</body>
</html>