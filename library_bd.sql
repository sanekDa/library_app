-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 21 2019 г., 18:39
-- Версия сервера: 5.6.41
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `library`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `patronymic` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`, `patronymic`) VALUES
(1, 'Александр', 'Пушкин', 'Сергеевич'),
(2, 'Иван', 'Тургенев', 'Сергеевич'),
(3, 'Михаил', 'Юрьевич', 'Лермонтов'),
(4, 'Фёдор', 'Достоевский', 'Михайлович'),
(5, 'Николай', 'Гоголь', 'Васильевич');

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `author_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `extra_information` varchar(255) NOT NULL,
  `availability` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`, `author_id`, `genre_id`, `extra_information`, `availability`) VALUES
(1, 'Дубровский', 1, 4, 'экстра', 1),
(2, 'Ася', 2, 1, 'экстра', 1),
(3, 'Мцыри', 3, 2, 'экстра', 1),
(4, 'Идиот', 4, 3, 'экстра', 1),
(5, 'Вий', 5, 5, 'экстра', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `books_deliveries`
--

CREATE TABLE `books_deliveries` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books_deliveries`
--

INSERT INTO `books_deliveries` (`id`, `delivery_id`, `book_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `patronymic` varchar(50) NOT NULL,
  `dbirth` date NOT NULL,
  `adress` varchar(50) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `firstname`, `lastname`, `patronymic`, `dbirth`, `adress`, `phone_number`, `email`) VALUES
(1, 'Олег', 'Орхипов', 'Алексеевич', '2018-06-01', 'Советская,21', '88889871212', 't11@mail.ru'),
(2, 'Тимур', 'Бекбек', 'Сергеевич', '2018-06-13', 'Советская,22', '88889871222', 't12@mail.ru'),
(3, 'Артадий', 'Песков', 'Алексеевич', '2018-06-02', 'Советская,23', '88889871232', 't13@mail.ru'),
(4, 'Татьяна', 'Корсакова', 'Александровна', '2018-06-04', 'Советская,24', '88889871242', 't41@mail.ru'),
(5, 'Александр', 'Тимофеев', 'Алексеевич', '2018-06-05', 'Советская,25', '88889871252', 't15@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `deliveries`
--

CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_of_delivery` date NOT NULL,
  `date_of_return` date NOT NULL,
  `worker_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `deliveries`
--

INSERT INTO `deliveries` (`id`, `client_id`, `date_of_delivery`, `date_of_return`, `worker_id`) VALUES
(1, 1, '2018-02-06', '2018-03-24', 1),
(2, 2, '2018-02-04', '2018-03-14', 2),
(3, 3, '2018-02-05', '2018-03-24', 3),
(4, 4, '2018-02-01', '2018-03-14', 5),
(5, 5, '2018-02-02', '2018-02-24', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `fines`
--

CREATE TABLE `fines` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `fines`
--

INSERT INTO `fines` (`id`, `delivery_id`, `description`, `price`) VALUES
(1, 1, 'Утеря', 1000),
(2, 2, 'Утеря', 1000),
(3, 3, 'Задержка', 500),
(4, 4, 'Утеря', 1000),
(5, 5, 'Утеря', 1500);

-- --------------------------------------------------------

--
-- Структура таблицы `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Фантастика'),
(2, 'Детектив'),
(3, 'Ужасы'),
(4, 'Трагедия'),
(5, 'Биография');

-- --------------------------------------------------------

--
-- Структура таблицы `workers`
--

CREATE TABLE `workers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `dbirth` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `patronymic` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `workers`
--

INSERT INTO `workers` (`id`, `firstname`, `lastname`, `phone_number`, `adress`, `dbirth`, `email`, `patronymic`) VALUES
(1, 'Иван', 'Васильев', '88889871211', 'Советская,2', '2018-06-03', 't1@mail.ru', 'Алексеевич'),
(2, 'Андрей', 'Петров', '88889871213', 'Советская,3', '2018-01-03', 't2@mail.ru', 'Сергеевич'),
(3, 'Александр', 'Павлов', '88889871214', 'Советская,4', '2018-02-03', 't3@mail.ru', 'Алексеевич'),
(4, 'Семен', 'Полынов', '88889871215', 'Советская,5', '2018-03-03', 't4@mail.ru', 'Александрович'),
(5, 'Артём', 'Кедрин', '88889874213', 'Советская,6', '2018-04-03', 't5@mail.ru', 'Алексеевич');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `books_deliveries`
--
ALTER TABLE `books_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fines`
--
ALTER TABLE `fines`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `books_deliveries`
--
ALTER TABLE `books_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `fines`
--
ALTER TABLE `fines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
