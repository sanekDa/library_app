<?php
switch ($_GET['action'])
{
  case 'add':
    $authors = $this->$pdo->query('SELECT * FROM `authors`');
    $url = '/index.php?function=authors&action=create';
    include 'forms/author.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `authors` (`firstname`, `lastname`, `patronymic`) VALUES (:firstname, :lastname, :patronymic)');
    $sql->execute([
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=authors">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `authors` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $author = $sql->fetch();
    $url = '/index.php?function=authors&action=update&id=' . $_GET['id'];
    include 'forms/author.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `authors` SET `firstname` = :firstname, `lastname` = :lastname, `patronymic` = :patronymic WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic'], 
    ]);
    echo 'Автор успешно обновлен!<br><a href="/index.php?function=authors">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `authors` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=authors">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=authors&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $authors = $this->$pdo->query('
      SELECT 
        `id`, 
        `firstname`, 
        `lastname`,  
        `patronymic`
      FROM 
        `authors`
      ');


    echo '<table border="1" cellspacing="0" class="table table-striped" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Имя</th>';
    echo '<th>Фамилия</th>';
    echo '<th>Отчество</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($authors as $author)
    {
      echo '<tr>';
      echo '<td>' . $author['id'] . '</td> ' 
      . '<td>' . $author['firstname'] . '</td> ' 
      . '<td>' . $author['lastname'] . '</td> ' 
      . '<td>' . $author['patronymic'] . '</td> ' 
      . '<td><a class="badge badge-info" href="/index.php?function=authors&action=edit&id=' . $author['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=authors&action=delete&id=' . $author['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

