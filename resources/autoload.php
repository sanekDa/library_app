<?php 
echo '<form action="" method="post" class="form-inline">
				<label>Сводка о выдаче</label><br>
				<input class="form-control" placeholder="Введите id выдачи" type="text" name="find_delivery" >
				<button type="submit" class="btn btn-primary">Найти</button><br><br>
			</form>';
if ($_POST){
	$delivery_view = $this->$pdo->query('
	      show tables
	');
	$is_exsists=0;
	foreach ($delivery_view as $key => $value)
	{
		if ($value[0]=='delivery_info'){
			$is_exsists=1;
			break;
		}
	}
	if ($is_exsists==0){
		$this->$pdo->query('
	      CREATE VIEW delivery_info 
	      AS SELECT `deliveries`.id  as `delivery_id`,
	      CONCAT_WS(" ", `clients`.`firstname`, `clients`.`lastname`) as `client_name`, 
		  `books`.name as `book_name`, 
		  CONCAT_WS(" ", `workers`.`firstname`, `workers`.`lastname`) as `worker_name`, 
		  `deliveries`.date_of_delivery as `date_of_delivery`,
		  `deliveries`.date_of_return as `date_of_return`
	      FROM `clients`,`books`,`workers`,`deliveries`,`books_deliveries` 
		  WHERE `deliveries`.id=`books_deliveries`.delivery_id and `books_deliveries`.book_id=`books`.id
		  and `clients`.id=`deliveries`.client_id and `deliveries`.worker_id=`workers`.id;
	     ');
	}
	$sql = $this->$pdo->prepare('SELECT * FROM delivery_info where delivery_id = :id');
    $sql->execute([':id' => $_POST['find_delivery']]);
    $delivery_info = $sql->fetch();
    echo '<table border="1" cellspacing="0" class="table table-striped" >';
    echo '<tr>';
    echo '<th>ID выдачи</th>';
    echo '<th>Имя читателя</th>';
    echo '<th>Имя работника</th>';
    echo '<th>Название книги</th>';
    echo '<th>Дата выдачи</th>';
    echo '<th>Дата возврата</th>';
    echo '</tr>';

      echo '<tr>';
      echo '<td>' . $delivery_info['delivery_id'] . '</td> ' 
      . '<td>' . $delivery_info['client_name']. '</td> '.
      '<td>' . $delivery_info['worker_name']. '</td> '.
      '<td>' . $delivery_info['book_name'] . '</td> '. 
      '<td>' . $delivery_info['date_of_delivery']. '</td> '. 
      '<td>' . $delivery_info['date_of_return'] . '</td> ';
      echo '</tr>';

    
    echo '</table>';
	echo '</table>';
	echo '<a class="badge badge-info" href="" style="margin-bottom:10%">Скрыть</a>';
}
echo '<ul class="list-group" >
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=authors">Список авторов</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=genres">Список жанров</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=books">Список книг</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=fines">Список штрафов</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=deliveries">Список выдач</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=delivered_books">Список выданных книг</a></li>

  <li class="list-group-item"><a class="list-group-item" href="index.php?function=clients">Список клиентов</a></li>
  
  <li class="list-group-item"><a class="list-group-item" href="index.php?function=workers">Список работников</a></li>
</ul>';
?>