<?php
switch ($_GET['action'])
{
  case 'add':
    $authors = $this->$pdo->query('SELECT * FROM `authors`');
    $genres = $this->$pdo->query('SELECT * FROM `genres`');
    $url = 'index.php?function=books&action=create';
    include 'forms/book.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `books` (`author_id`, `genre_id`, `name`, `extra_information`,`availability`) VALUES (:author_id, :genre_id, :name, :extra_information, :availability)');
    $sql->execute([
      ':author_id' => $_POST['author_id'], 
      ':genre_id' => $_POST['genre_id'], 
      ':name' => $_POST['name'], 
      ':availability' => $_POST['availability'], 
      ':extra_information' => $_POST['extra_information']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=books">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `books` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $book = $sql->fetch();

    $authors = $this->$pdo->query('SELECT * FROM `authors`');
    $genres = $this->$pdo->query('SELECT * FROM `genres`');

    $url = 'index.php?function=books&action=update&id=' . $_GET['id'];
    include 'forms/book.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `books` SET `author_id` = :author_id, `genre_id` = :genre_id, `name` = :name, `availability` = :availability, `extra_information` = :extra_information WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':author_id' => $_POST['author_id'], 
      ':genre_id' => $_POST['genre_id'], 
      ':name' => $_POST['name'], 
      ':availability' => $_POST['availability'], 
      ':extra_information' => $_POST['extra_information']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=books">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `books` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=books">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="index.php?function=books&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $books = $this->$pdo->query('
      SELECT 
        `b`.`id`, 
        `b`.`name`, 
        CONCAT_WS(" ", `a`.`firstname`, `a`.`lastname`) `author_name`, 
        `g`.name `genre`,
        `b`.`extra_information` `extra_information`,
        `b`.`availability` `availability`
      FROM 
        `books` `b`, 
        `genres` `g`, 
        `authors` `a` 
      WHERE 
        `b`.`author_id` = `a`.`id` 
        AND
        `b`.`genre_id` = `g`.`id`
       ORDER BY `id` ASC
      ');


    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Наименование</th>';
    echo '<th>Автор</th>';
    echo '<th>Жанр</th>';
    echo '<th>Доп. инф-ция</th>';
    echo '<th>Наличие</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($books as $book)
    {
      echo '<tr>';
      echo '<td>' . $book['id'] . '</td> ' 
      . '<td>' . $book['name'] . '</td> ' 
      . '<td>' . $book['author_name'] . '</td> ' 
      . '<td>' . $book['genre'] . '</td> ',
        '<td>' . $book['extra_information'] . '</td> ',
        '<td>' . $book['availability'] . '</td> ',
        '<td><a class="badge badge-info" href="index.php?function=books&action=edit&id=' . $book['id'] . '">ред.</a> <a class="badge badge-danger" href="index.php?function=books&action=delete&id=' . $book['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

