
<?php



switch ($_GET['action'])
{
  case 'add':
    $url = '/index.php?function=clients&action=create';
    include 'forms/client.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `clients` (`firstname`, `lastname`, `patronymic`, `dbirth`, `adress`, `phone_number`,`email`) VALUES (:firstname, :lastname, :patronymic, :dbirth, :adress, :phone_number, :email)');
    $sql->execute([
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic'], 
      ':dbirth' => $_POST['dbirth'],
      ':adress' => $_POST['adress'], 
      ':phone_number' => $_POST['phone_number'], 
      ':email' => $_POST['email']
    ]);
    echo 'Успешно добавлено!<br><a href="/index.php?function=clients">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `clients` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $client = $sql->fetch();

    $url = '/index.php?function=clients&action=update&id=' . $_GET['id'];
    include 'forms/client.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `clients` SET `firstname` = :firstname, `lastname` = :lastname, `patronymic` = :patronymic, `dbirth` = :dbirth, `adress` = :adress, `phone_number` = :phone_number, `email` = :email WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic'], 
      ':dbirth' => $_POST['dbirth'],
      ':adress' => $_POST['adress'], 
      ':phone_number' => $_POST['phone_number'], 
      ':email' => $_POST['email']
    ]);
    echo 'Оценка успешно обновлена!<br><a href="/index.php?function=clients">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `clients` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=clients">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=clients&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $clients = $this->$pdo->query('
      SELECT 
        `id`, 
        CONCAT_WS(" ", `firstname`, `lastname`,`patronymic`) `fio`,
        `phone_number`, 
        `adress`, 
        `dbirth`, 
        `email` 
      FROM 
        `clients`
      ORDER BY `id` ASC
');


    echo '<table border="1" cellspacing="0" class="table table-striped" >';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>ФИО</th>';
    echo '<th>Телефон</th>';
    echo '<th>Адрес</th>';
    echo '<th>Дата рождения</th>';
    echo '<th>E-mail</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($clients as $client)
    {
      echo '<tr>';

      echo '<td>' . $client['id'] . '</td> ' 
      . '<td>' . $client['fio'] . '</td> ' 
      . '<td>' . $client['phone_number'] . '</td> ' 
      . '<td>' . $client['adress'] . '</td> ' 
      . '<td>' . $client['dbirth'] . '</td> ' 
      . '<td>' . $client['email'] . '</td> ' 
      . '<td><a class="badge badge-info" href="/index.php?function=clients&action=edit&id=' . $client['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=clients&action=delete&id=' . $client['id'] . '">уд.</a></td>';
      
      echo '</tr>';

    }
    echo '</table>';

  break;

}

