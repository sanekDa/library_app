<?php
switch ($_GET['action'])
{
  case 'add':
    $deliveries = $this->$pdo->query('SELECT * FROM `deliveries`');
    $books = $this->$pdo->query('SELECT * FROM `books` where availability=1');
    $url = '/index.php?function=delivered_books&action=create';
    include 'forms/delivered_book.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `books_deliveries` (`id`,`delivery_id`, `book_id`) VALUES (:id,:delivery_id, :book_id)');
    $sql->execute([
      ':id' => $new_id,
      ':delivery_id' => $_POST['delivery_id'], 
      ':book_id' => $_POST['book_id']
    ]);
    $sql = $this->$pdo->prepare('UPDATE `books` SET `availability` = 0 
     WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_POST['book_id'],
    ]);
    echo 'Успешно добавлено!<br><a href="/index.php?function=delivered_books">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `books_deliveries` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $item = $sql->fetch();

    $deliveries = $this->$pdo->query('SELECT * FROM `deliveries`');
    $books = $this->$pdo->query('SELECT * FROM `books`');

    $url = '/index.php?function=delivered_books&action=update&id=' . $_GET['id'];
    include 'forms/delivered_book.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `books_deliveries` SET `delivery_id` = :delivery_id, 
      `book_id` = :book_id WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':delivery_id' => $_POST['delivery_id'], 
      ':book_id' => $_POST['book_id']
    ]);
    
    echo 'Успешно!<br><a href="/index.php?function=delivered_books">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('SELECT `book_id` FROM `books_deliveries` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $book_id = $sql->fetch();
    $sql = $this->$pdo->prepare('DELETE FROM `books_deliveries` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    $sql = $this->$pdo->prepare('UPDATE `books` SET `availability` = 1  WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $book_id['book_id'],
    ]);
    echo 'Удалено!<br><a href="/index.php?function=delivered_books">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=delivered_books&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $books_deliveries = $this->$pdo->query('
      SELECT * FROM `books_deliveries` ORDER BY `delivery_id` ASC
      ');


    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID выдачи</th>';
    echo '<th>ID книги</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($books_deliveries as $item)
    {
      echo '<tr>';
      echo '<td>' . $item['delivery_id'] . '</td> ' 
      . '<td>' . $item['book_id'] . '</td> '. 
        '<td><a class="badge badge-info" href="/index.php?function=delivered_books&action=edit&id=' . $item['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=delivered_books&action=delete&id=' . $item['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

