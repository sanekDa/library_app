<?php
switch ($_GET['action'])
{
  case 'add':
    $clients = $this->$pdo->query('SELECT * FROM `clients`');
    $workers = $this->$pdo->query('SELECT * FROM `workers`');

    $url = '/index.php?function=deliveries&action=create';
    include 'forms/delivery.php';
  break;

  case 'create':

    $sql = $this->$pdo->prepare('INSERT INTO `deliveries` (`client_id`, `worker_id`, `date_of_delivery`, `date_of_return`) VALUES (:client_id, :worker_id, :date_of_delivery, :date_of_return)');
    $sql->execute([
      ':client_id' => $_POST['client_id'], 
      ':worker_id' => $_POST['worker_id'], 
      ':date_of_delivery' => $_POST['date_of_delivery'], 
      ':date_of_return' => $_POST['date_of_return']
    ]);
    echo 'Успешно добавлено!<br><a href="/index.php?function=deliveries">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `deliveries` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $delivery = $sql->fetch();
    $clients = $this->$pdo->query('SELECT * FROM `clients`');
    $workers = $this->$pdo->query('SELECT * FROM `workers`');

    $url = '/index.php?function=deliveries&action=update&id=' . $_GET['id'];
    include 'forms/delivery.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `deliveries` SET `client_id` = :client_id, `worker_id` = :worker_id, `date_of_delivery` = :date_of_delivery, `date_of_return` = :date_of_return WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':client_id' => $_POST['client_id'], 
      ':worker_id' => $_POST['worker_id'], 
      ':date_of_delivery' => $_POST['date_of_delivery'], 
      ':date_of_return' => $_POST['date_of_return']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=deliveries">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `deliveries` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=deliveries">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=deliveries&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $deliveries = $this->$pdo->query('
      SELECT 
        `d`.`id`,
         CONCAT_WS(" ", `c`.`firstname`, `c`.`lastname`) `client_name`,
        `d`.`date_of_delivery`, 
        `d`.`date_of_return`, 
         CONCAT_WS(" ", `w`.`firstname`, `w`.`lastname`) `worker_name`
      FROM 
        `deliveries` `d`, 
        `clients` `c`, 
        `workers` `w` 
      WHERE 
        `d`.`client_id` = `c`.`id` 
        AND
        `d`.`worker_id` = `w`.`id`
      ORDER BY `id` ASC
');


    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Читатель</th>';
    echo '<th>Дата выдачи</th>';
    echo '<th>Дата возврата</th>';
    echo '<th>Имя работника</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($deliveries as $delivery)
    {
      echo '<tr>';
      echo '<td>' . $delivery['id'] . '</td> ' 
      . '<td>' . $delivery['client_name'] . '</td> ' 
      . '<td>' . $delivery['date_of_delivery'] . '</td> ' 
      . '<td>' . $delivery['date_of_return'] . '</td> ' 
      . '<td>' . $delivery['worker_name'] . '</td> ' 
      . '<td><a class="badge badge-success" href="/index.php?function=deliveries&action=edit&id=' . $delivery['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=deliveries&action=delete&id=' . $delivery['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

