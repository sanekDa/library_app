<?php
switch ($_GET['action'])
{
  case 'add':
    $url = '/index.php?function=fines&action=create';
    $deliveries = $this->$pdo->query('SELECT * FROM `deliveries`');
    include 'forms/fine.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `fines` (delivery_id,`description`,`price`) VALUES (:delivery_id,:description,:price)');
    $sql->execute([
      ':delivery_id' => $_POST['delivery_id'],
      ':description' => $_POST['description'],
      ':price' => $_POST['price'],
    ]);
    echo 'Успешно!<br><a href="/index.php?function=fines">Назад</a>';
  break;
  
  case 'edit':
  	$deliveries = $this->$pdo->query('SELECT * FROM `deliveries`');
    $sql = $this->$pdo->prepare('SELECT * FROM `fines` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $fine = $sql->fetch();
    $url = '/index.php?function=fines&action=update&id=' . $_GET['id'];
    include 'forms/fine.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `fines` SET `delivery_id` = :delivery_id, `description` = :description, `price` = :price WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':delivery_id' => $_POST['delivery_id'],
      ':description' => $_POST['description'],
      ':price' => $_POST['price'],
    ]);
    echo 'Успешно!<br><a href="/index.php?function=fines">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `fines` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=fines">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=fines&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';


    $fines = $this->$pdo->query('SELECT * FROM `fines` ORDER BY `delivery_id` ASC');
    
    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID доставки</th>';
    echo '<th>Описание</th>';
    echo '<th>Сумма</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($fines as $fine)
    {
      echo '<tr>';
      echo '<td>' . $fine['delivery_id'] . '</td> ' 
      . '<td>' . $fine['description'] . '</td> '
      . '<td>' . $fine['price'] . '</td> '
      . '<td><a class="badge badge-info" href="/index.php?function=fines&action=edit&id=' . $fine['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=fines&action=delete&id=' . $fine['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

