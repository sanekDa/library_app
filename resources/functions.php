<?php
class Functions {
	public $pdo;
	public function __construct() {
        $this->$pdo = new PDO('mysql:host=127.0.0.1;dbname=library', 'root', '');
		$this->$pdo->exec("set names utf8");
    }
    public function show_authors_list(){
    	include 'authors.php';
    }
    public function show_genres_list(){
    	include 'genres.php';
    }
    public function show_fines_list(){
    	include 'fines.php';
    }
    public function show_delivered_books_list(){
    	include 'delivered_books.php';
    }
    public function show_deliveries_list(){
    	include 'deliveries.php';
    }
    public function show_clients_list(){
    	include 'clients.php';
    }
    public function show_workers_list(){
    	include 'workers.php';
    }
    public function show_books_list(){
    	include 'books.php';
    }
    public function show_main_list(){
    	include 'autoload.php';
    }
}
$Functions = new Functions($Configuration);
?>