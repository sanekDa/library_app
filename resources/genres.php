<?php


switch ($_GET['action'])
{
  case 'add':
    $url = '/index.php?function=genres&action=create';
    include 'forms/genre.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `genres` (`name`) VALUES (:name)');
    $sql->execute([
      ':name' => $_POST['name'],
    ]);
    echo 'Успешно!<br><a href="/index.php?function=genres">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `genres` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $genre = $sql->fetch();
    $url = '/index.php?function=genres&action=update&id=' . $_GET['id'];
    include 'forms/genre.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `genres` SET `name` = :name WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':name' => $_POST['name']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=genres">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `genres` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=genres">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=genres&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $genres = $this->$pdo->query('SELECT * FROM `genres`');

    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Название</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($genres as $genre)
    {
      echo '<tr>';
      echo '<td>' . $genre['id'] . '</td> ' 
      . '<td>' . $genre['name'] . '</td> ' 
      . '<td><a class="badge badge-info" href="/index.php?function=genres&action=edit&id=' . $genre['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=genres&action=delete&id=' . $genre['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

