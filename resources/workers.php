<?php
switch ($_GET['action'])
{
  case 'add':
    $url = '/index.php?function=workers&action=create';
    include 'forms/worker.php';
  break;

  case 'create':
    $sql = $this->$pdo->prepare('INSERT INTO `workers` (`firstname`, `lastname`, `patronymic`, `dbirth`, `adress`, `phone_number`,`email`) VALUES (:firstname, :lastname, :patronymic, :dbirth, :adress, :phone_number, :email)');
    $sql->execute([
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic'], 
      ':dbirth' => $_POST['dbirth'],
      ':adress' => $_POST['adress'], 
      ':phone_number' => $_POST['phone_number'], 
      ':email' => $_POST['email']
    ]);
    echo 'Успешно добавлено!<br><a href="/index.php?function=workers">Назад</a>';
  break;
  
  case 'edit':
    $sql = $this->$pdo->prepare('SELECT * FROM `workers` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $worker = $sql->fetch();

    $url = '/index.php?function=workers&action=update&id=' . $_GET['id'];
    include 'forms/worker.php';
  break;
  
  case 'update':
    $sql = $this->$pdo->prepare('UPDATE `workers` SET `firstname` = :firstname, `lastname` = :lastname, `patronymic` = :patronymic, `dbirth` = :dbirth, `adress` = :adress, `phone_number` = :phone_number, `email` = :email WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':firstname' => $_POST['firstname'], 
      ':lastname' => $_POST['lastname'], 
      ':patronymic' => $_POST['patronymic'], 
      ':dbirth' => $_POST['dbirth'],
      ':adress' => $_POST['adress'], 
      ':phone_number' => $_POST['phone_number'], 
      ':email' => $_POST['email']
    ]);
    echo 'Успешно!<br><a href="/index.php?function=workers">Назад</a>';
  break;

  case 'delete':
    $sql = $this->$pdo->prepare('DELETE FROM `workers` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/index.php?function=workers">Назад</a>';
  break;

  default:

    echo '<a href="/" class="badge badge-dark" style="margin-top:1%; margin-top; padding:1%;">Вернуться на главную</a><hr>';

    echo '<a href="/index.php?function=workers&action=add" class="badge badge-dark" style="margin-bottom:1%; padding:1%;">Добавить</a><br>';

    $workers = $this->$pdo->query('
      SELECT 
        `id`, 
        CONCAT_WS(" ", `firstname`, `lastname`,`patronymic`) `fio`,
        `phone_number`, 
        `adress`, 
        `dbirth`, 
        `email` 
      FROM 
        `workers`
      ORDER BY `id` ASC
');


    echo '<table border="1" cellspacing="0" class="table table-striped">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>ФИО</th>';
    echo '<th>Телефон</th>';
    echo '<th>Адрес</th>';
    echo '<th>Дата рождения</th>';
    echo '<th>E-mail</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($workers as $worker)
    {
      echo '<tr>';
      echo '<td>' . $worker['id'] . '</td> ' 
      . '<td>' . $worker['fio'] . '</td> ' 
      . '<td>' . $worker['phone_number'] . '</td> ' 
      . '<td>' . $worker['adress'] . '</td> ' 
      . '<td>' . $worker['dbirth'] . '</td> ' 
      . '<td>' . $worker['email'] . '</td> ' 
      . '<td><a class="badge badge-info" href="/index.php?function=workers&action=edit&id=' . $worker['id'] . '">ред.</a> <a class="badge badge-danger" href="/index.php?function=workers&action=delete&id=' . $worker['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

